var Student = /** @class */ (function () {
    function Student(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.fullname = firstname + " " + lastname;
    }
    return Student;
}());
function greeter(person) {
    return "hello " + person.firstname + " " + person.lastname;
}
//let user = {firstname:"Andrea", lastname:"Zaccaro"}
var user = new Student("Andrea", "Zaccaro");
document.body.innerHTML = greeter(user);
