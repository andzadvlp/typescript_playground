class Student{
  fullname: string;
  constructor(
    public firstname:string,
    public lastname: string
  ){
    this.fullname = firstname + " " + lastname;
  }
}

interface Person{
  firstname : string;
  lastname : string;
}


function greeter(person : Person){
  return "hello " + person.firstname + " " + person.lastname;
}

//let user = {firstname:"Andrea", lastname:"Zaccaro"}
let user = new Student("Andrea","Zaccaro");
document.body.innerHTML=greeter(user);
